package ru.t1.kharitonova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.kharitonova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.kharitonova.tm.api.endpoint.IProjectEndpoint;
import ru.t1.kharitonova.tm.api.service.IPropertyService;
import ru.t1.kharitonova.tm.dto.request.project.*;
import ru.t1.kharitonova.tm.dto.request.user.UserLoginRequest;
import ru.t1.kharitonova.tm.dto.response.project.ProjectCreateResponse;
import ru.t1.kharitonova.tm.dto.response.project.ProjectShowByIdResponse;
import ru.t1.kharitonova.tm.dto.response.project.ProjectUpdateByIdResponse;
import ru.t1.kharitonova.tm.dto.response.user.UserLoginResponse;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.marker.SoapCategory;
import ru.t1.kharitonova.tm.dto.model.ProjectDTO;
import ru.t1.kharitonova.tm.service.PropertyService;

import java.util.List;

@Category(SoapCategory.class)
public class ProjectEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(
            propertyService.getHost(), propertyService.getPort()
    );

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(
            propertyService.getHost(), propertyService.getPort()
    );

    @Nullable
    private String token;

    @Before
    public void initTest() {
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(
                new UserLoginRequest("test", "test")
        );
        token = loginResponse.getToken();
    }

    @Test
    public void testChangeProjectStatusById() {
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(
                new ProjectCreateRequest(token, "Test Project", "Test Description")
        );
        Assert.assertNotNull(response);
        @Nullable ProjectDTO projectDTO = response.getProjectDTO();
        Assert.assertNotNull(projectDTO);
        @Nullable final String projectId = projectDTO.getId();
        @Nullable final Status status = projectDTO.getStatus();
        Assert.assertEquals(Status.NOT_STARTED, status);
        @NotNull final Status newStatus = Status.IN_PROGRESS;
        projectDTO = projectEndpoint.changeProjectStatusById(
                new ProjectChangeStatusByIdRequest(token, projectId, newStatus)
        ).getProjectDTO();
        Assert.assertNotNull(projectDTO);
        Assert.assertEquals(Status.IN_PROGRESS, projectDTO.getStatus());
        projectEndpoint.clearProject(new ProjectClearRequest(token));
    }

    @Test
    public void testClearProject() {
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        projectEndpoint.createProject(
                new ProjectCreateRequest(token, "Test Project", "Test Description")
        );
        @Nullable List<ProjectDTO> projectDTOS = projectEndpoint.listProject(
                new ProjectListRequest(token)
        ).getProjectDTOS();
        Assert.assertNotNull(projectDTOS);
        Assert.assertTrue(projectDTOS.size() > 0);
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        projectDTOS = projectEndpoint.listProject(new ProjectListRequest(token)).getProjectDTOS();
        Assert.assertNull(projectDTOS);
    }

    @Test
    public void testCreateProject() {
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(
                new ProjectCreateRequest(token, "Test Project", "Test Description")
        );
        Assert.assertNotNull(response);
        @Nullable ProjectDTO projectDTO = response.getProjectDTO();
        Assert.assertNotNull(projectDTO);
        projectEndpoint.clearProject(new ProjectClearRequest(token));
    }

    @Test
    public void testShowProjectById() {
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(
                new ProjectCreateRequest(token, "Test Project", "Test Description")
        );
        Assert.assertNotNull(response);
        @Nullable ProjectDTO projectDTO = response.getProjectDTO();
        Assert.assertNotNull(projectDTO);
        @NotNull String projectId = projectDTO.getId();
        @NotNull final ProjectShowByIdResponse projectGetByIdResponse = projectEndpoint.showProjectById(
                new ProjectShowByIdRequest(token, projectId)
        );
        Assert.assertNotNull(projectGetByIdResponse);
        Assert.assertNotNull(projectGetByIdResponse.getProjectDTO());
        Assert.assertEquals("Test Project", projectGetByIdResponse.getProjectDTO().getName());
        projectEndpoint.clearProject(new ProjectClearRequest(token));
    }

    @Test
    public void testListProject() {
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        projectEndpoint.createProject(new ProjectCreateRequest(
                token, "Test Project", "Test Description")
        );
        projectEndpoint.createProject(new ProjectCreateRequest(
                token, "Test2 Project", "Test2 Description2")
        );
        @Nullable List<ProjectDTO> projectDTOS = projectEndpoint.listProject(
                new ProjectListRequest(token)).getProjectDTOS();
        Assert.assertNotNull(projectDTOS);
        Assert.assertEquals(2, projectDTOS.size());
        projectEndpoint.clearProject(new ProjectClearRequest(token));
    }

    @Test
    public void testRemoveProjectById() {
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        projectEndpoint.createProject(new ProjectCreateRequest(
                token, "Test Project", "Test Description")
        );
        @Nullable List<ProjectDTO> projectDTOS = projectEndpoint.listProject(
                new ProjectListRequest(token)).getProjectDTOS();
        Assert.assertNotNull(projectDTOS);
        Assert.assertEquals(1, projectDTOS.size());
        @NotNull final String projectId1 = projectDTOS.get(0).getId();
        projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(token, projectId1));
        projectDTOS = projectEndpoint.listProject(new ProjectListRequest(token)).getProjectDTOS();
        Assert.assertNull(projectDTOS);
        projectEndpoint.clearProject(new ProjectClearRequest(token));
    }

    @Test
    public void testUpdateProjectById() {
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(
                new ProjectCreateRequest(token, "Test Project", "Test Description")
        );
        @Nullable ProjectDTO projectDTO = response.getProjectDTO();
        Assert.assertNotNull(projectDTO);
        @Nullable String projectId = projectDTO.getId();
        Assert.assertEquals("Test Project", projectDTO.getName());
        @NotNull final ProjectUpdateByIdResponse projectUpdateByIdResponse = projectEndpoint.updateProjectById(
                new ProjectUpdateByIdRequest(token, projectId, "Test Project new", "Test Description New")
        );
        Assert.assertNotNull(projectUpdateByIdResponse);
        Assert.assertNotNull(projectUpdateByIdResponse.getProjectDTO());
        Assert.assertEquals(projectId, projectUpdateByIdResponse.getProjectDTO().getId());
        Assert.assertEquals("Test Project new", projectUpdateByIdResponse.getProjectDTO().getName());
        Assert.assertEquals("Test Description New", projectUpdateByIdResponse.getProjectDTO().getDescription());
        projectEndpoint.clearProject(new ProjectClearRequest(token));
    }

    @Test
    public void testStartProjectById() {
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(
                new ProjectCreateRequest(token, "Test Project", "Test Description")
        );
        Assert.assertNotNull(response);
        @Nullable ProjectDTO projectDTO = response.getProjectDTO();
        Assert.assertNotNull(projectDTO);
        @Nullable final String projectId = projectDTO.getId();
        @Nullable final Status status = projectDTO.getStatus();
        Assert.assertEquals(Status.NOT_STARTED, status);
        projectDTO = projectEndpoint.startProjectById(
                new ProjectStartByIdRequest(token, projectId)
        ).getProjectDTO();
        Assert.assertNotNull(projectDTO);
        Assert.assertEquals(Status.IN_PROGRESS, projectDTO.getStatus());
        projectEndpoint.clearProject(new ProjectClearRequest(token));
    }

    @Test
    public void testCompleteProjectById() {
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(
                new ProjectCreateRequest(token, "Test Project", "Test Description")
        );
        Assert.assertNotNull(response);
        @Nullable ProjectDTO projectDTO = response.getProjectDTO();
        Assert.assertNotNull(projectDTO);
        @Nullable final String projectId = projectDTO.getId();
        @Nullable final Status status = projectDTO.getStatus();
        Assert.assertEquals(Status.NOT_STARTED, status);
        projectDTO = projectEndpoint.completeProjectById(new ProjectCompleteByIdRequest(token, projectId)).getProjectDTO();
        Assert.assertNotNull(projectDTO);
        Assert.assertEquals(Status.COMPLETED, projectDTO.getStatus());
        projectEndpoint.clearProject(new ProjectClearRequest(token));
    }

}
