package ru.t1.kharitonova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.kharitonova.tm.api.endpoint.IUserEndpoint;
import ru.t1.kharitonova.tm.command.AbstractCommand;
import ru.t1.kharitonova.tm.exception.user.UserNotFoundException;
import ru.t1.kharitonova.tm.dto.model.UserDTO;

public abstract class AbstractUserCommand extends AbstractCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    public IUserEndpoint getUserEndpoint() {
        return serviceLocator.getUserEndpoint();
    }

    @NotNull
    public IAuthEndpoint getAuthEndpoint() {
        return serviceLocator.getAuthEndpoint();
    }

    protected void showUser(@Nullable final UserDTO userDTO) {
        if (userDTO == null) throw new UserNotFoundException();
        System.out.println("LOGIN: " + userDTO.getLogin());
        System.out.println("E-MAIL: " + userDTO.getEmail());
        System.out.println("ROLE: " + userDTO.getRole().getDisplayName());
    }

}
