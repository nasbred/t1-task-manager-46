package ru.t1.kharitonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    IAuthEndpoint getAuthEndpoint();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    ISystemEndpoint getSystemEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

    @NotNull
    IDomainEndpoint getDomainEndpoint();

    @NotNull
    ITokenService getTokenService();

}
