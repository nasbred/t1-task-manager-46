package ru.t1.kharitonova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.kharitonova.tm.api.service.IConnectionService;
import ru.t1.kharitonova.tm.api.service.IPropertyService;
import ru.t1.kharitonova.tm.api.service.model.IProjectService;
import ru.t1.kharitonova.tm.api.service.model.IProjectTaskService;
import ru.t1.kharitonova.tm.api.service.model.ITaskService;
import ru.t1.kharitonova.tm.api.service.model.IUserService;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kharitonova.tm.exception.entity.TaskNotFoundException;
import ru.t1.kharitonova.tm.exception.field.ProjectIdEmptyException;
import ru.t1.kharitonova.tm.exception.field.TaskIdEmptyException;
import ru.t1.kharitonova.tm.exception.field.UserIdEmptyException;
import ru.t1.kharitonova.tm.marker.ServerCategory;
import ru.t1.kharitonova.tm.model.Project;
import ru.t1.kharitonova.tm.model.Task;
import ru.t1.kharitonova.tm.model.User;
import ru.t1.kharitonova.tm.service.ConnectionService;
import ru.t1.kharitonova.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(ServerCategory.class)
public class ProjectTaskServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    protected final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService, projectService, taskService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService, projectService, taskService);

    @NotNull
    private List<Project> projectList;

    @NotNull
    private List<Task> taskList;

    @NotNull
    private List<User> userList;

    @NotNull
    private final String projectName = "Project Name Test";

    @NotNull
    private final String taskName = "Task Name Test";

    @NotNull
    private final String description = "Description";

    @Before
    public void init() {
        projectList = new ArrayList<>();
        taskList = new ArrayList<>();
        userList = new ArrayList<>();

        if (userService.findByLogin("admin") == null) {
            @NotNull final User userAdmin = new User();
            userAdmin.setLogin("admin");
            userAdmin.setPasswordHash("admin");
            userAdmin.setEmail("admin@test.ru");
            userAdmin.setRole(Role.ADMIN);
            userService.add(userAdmin);
        }
        if (userService.findByLogin("test") == null) {
            @NotNull final User userTest = new User();
            userTest.setLogin("test");
            userTest.setPasswordHash("test");
            userTest.setEmail("test@test.ru");
            userTest.setRole(Role.USUAL);
            userService.add(userTest);
        }
        @Nullable User userAdminBase = userService.findByLogin("admin");
        @Nullable User userTestBase = userService.findByLogin("test");
        userList.add(userAdminBase);
        userList.add(userTestBase);

        projectList.add(new Project(userAdminBase, "Test Project 1", "Description 1 admin", Status.IN_PROGRESS));
        projectList.add(new Project(userAdminBase, "Test Project 2", "Description 2 admin", Status.NOT_STARTED));
        projectList.add(new Project(userTestBase, "Test Project 3", "Description 3 user", Status.IN_PROGRESS));
        projectList.add(new Project(userTestBase, "Test Project 4", "Description 4 user", Status.NOT_STARTED));

        taskList.add(new Task(userAdminBase, "Test Task 1", "Description 1 admin", Status.NOT_STARTED, projectList.get(0)));
        taskList.add(new Task(userAdminBase, "Test Task 2", "Description 2 admin", Status.IN_PROGRESS, projectList.get(1)));
        taskList.add(new Task(userTestBase, "Test Task 3", "Description 3 user", Status.IN_PROGRESS, projectList.get(2)));
        taskList.add(new Task(userTestBase, "Test Task 4", "Description 4 user", Status.IN_PROGRESS, null));

        taskService.removeAll();
        projectService.removeAll();
        for (@NotNull final Project project : projectList) projectService.add(project);
        for (@NotNull final Task task : taskList) taskService.add(task);
    }

    @After
    public void clear() {
        taskService.removeAll();
        projectService.removeAll();
        taskList.clear();
        userList.clear();
    }

    @Test
    public void testBindTaskToProject() {
        @NotNull final String user_test_id = userList.get(0).getId();
        @NotNull final Project project = projectService.create(user_test_id, projectName, description);
        @NotNull final String projectId = project.getId();
        @Nullable Task task = taskService.create(user_test_id, taskName, description);
        @NotNull final String taskId = task.getId();

        projectTaskService.bindTaskToProject(user_test_id, projectId, taskId);
        @Nullable Task updatedTask = taskService.findOneById(user_test_id, taskId);
        Assert.assertNotNull(updatedTask);
        Assert.assertEquals(updatedTask.getProject().getId(), projectId);

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject("", projectId, taskId)
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(null, projectId, taskId)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(user_test_id, "", taskId)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(user_test_id, null, taskId)
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(user_test_id, projectId, "")
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(user_test_id, projectId, null)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(user_test_id, UUID.randomUUID().toString(), taskId)
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(user_test_id, projectId, UUID.randomUUID().toString())
        );
    }

    @Test
    public void testRemoveProjectById() {
        @NotNull final String user_test_id = userList.get(0).getId();
        @NotNull final Project project = projectService.create(user_test_id, projectName, description);
        @NotNull final String projectId = project.getId();
        @NotNull final Task task = taskService.create(user_test_id, taskName, description);
        @NotNull final String taskId = task.getId();
        projectTaskService.bindTaskToProject(user_test_id, projectId, taskId);
        projectTaskService.removeProjectById(user_test_id, projectId);
        Assert.assertNull(projectService.findOneById(user_test_id, projectId));
        Assert.assertNull(taskService.findOneById(user_test_id, taskId));

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.removeProjectById("", projectId)
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.removeProjectById(null, projectId)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.removeProjectById(user_test_id, null)
        );
    }

    @Test
    public void testUnbindTaskFromProject() {
        @NotNull final String user_test_id = userList.get(0).getId();
        @NotNull final Project project = projectService.create(user_test_id, projectName, description);
        @NotNull final String projectId = project.getId();
        @NotNull final Task task = taskService.create(user_test_id, taskName, description);
        @NotNull final String taskId = task.getId();
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject("", projectId, taskId)
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(null, projectId, taskId)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(user_test_id, "", taskId)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(user_test_id, null, taskId)
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(user_test_id, projectId, "")
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(user_test_id, projectId, null)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> projectTaskService.unbindTaskFromProject(user_test_id, UUID.randomUUID().toString(), taskId)
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> projectTaskService.unbindTaskFromProject(user_test_id, projectId, UUID.randomUUID().toString())
        );
        projectTaskService.unbindTaskFromProject(user_test_id, projectId, taskId);
        Assert.assertNotNull(taskService.findOneById(user_test_id, taskId));
        Assert.assertNull(task.getProject());
    }

}
