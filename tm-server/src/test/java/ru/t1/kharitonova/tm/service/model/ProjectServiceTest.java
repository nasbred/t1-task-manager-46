package ru.t1.kharitonova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.kharitonova.tm.api.service.IConnectionService;
import ru.t1.kharitonova.tm.api.service.IPropertyService;
import ru.t1.kharitonova.tm.api.service.model.IProjectService;
import ru.t1.kharitonova.tm.api.service.model.ITaskService;
import ru.t1.kharitonova.tm.api.service.model.IUserService;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kharitonova.tm.exception.field.IndexIncorrectException;
import ru.t1.kharitonova.tm.exception.field.NameEmptyException;
import ru.t1.kharitonova.tm.exception.field.UserIdEmptyException;
import ru.t1.kharitonova.tm.marker.ServerCategory;
import ru.t1.kharitonova.tm.model.Project;
import ru.t1.kharitonova.tm.model.Task;
import ru.t1.kharitonova.tm.model.User;
import ru.t1.kharitonova.tm.service.ConnectionService;
import ru.t1.kharitonova.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Category(ServerCategory.class)
public class ProjectServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    protected final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService, projectService, taskService);

    @NotNull
    private List<Project> projectList;

    @NotNull
    private List<Task> taskList;

    @NotNull
    private List<User> userList;

    @Before
    public void init() {
        projectList = new ArrayList<>();
        taskList = new ArrayList<>();
        userList = new ArrayList<>();

        if (userService.findByLogin("admin") == null) {
            @NotNull final User userAdmin = new User();
            userAdmin.setLogin("admin");
            userAdmin.setPasswordHash("admin");
            userAdmin.setEmail("admin@test.ru");
            userAdmin.setRole(Role.ADMIN);
            userService.add(userAdmin);
        }
        if (userService.findByLogin("test") == null) {
            @NotNull final User userTest = new User();
            userTest.setLogin("test");
            userTest.setPasswordHash("test");
            userTest.setEmail("test@test.ru");
            userTest.setRole(Role.USUAL);
            userService.add(userTest);
        }
        @Nullable User userAdminBase = userService.findByLogin("admin");
        @Nullable User userTestBase = userService.findByLogin("test");
        userList.add(userAdminBase);
        userList.add(userTestBase);

        projectList.add(new Project(userAdminBase, "Test Project 1", "Description 1 admin", Status.IN_PROGRESS));
        projectList.add(new Project(userAdminBase, "Test Project 2", "Description 2 admin", Status.NOT_STARTED));
        projectList.add(new Project(userTestBase, "Test Project 3", "Description 3 user", Status.IN_PROGRESS));
        projectList.add(new Project(userTestBase, "Test Project 4", "Description 4 user", Status.NOT_STARTED));

        taskList.add(new Task(userAdminBase, "Test Task 1", "Description 1 admin", Status.NOT_STARTED, projectList.get(0)));
        taskList.add(new Task(userAdminBase, "Test Task 2", "Description 2 admin", Status.IN_PROGRESS, projectList.get(1)));
        taskList.add(new Task(userTestBase, "Test Task 3", "Description 3 user", Status.IN_PROGRESS, projectList.get(2)));
        taskList.add(new Task(userTestBase, "Test Task 4", "Description 4 user", Status.IN_PROGRESS, null));

        projectService.removeAll();
        taskService.removeAll();
        for (@NotNull final Project project : projectList) projectService.add(project);
        for (@NotNull final Task task : taskList) taskService.add(task);
    }

    @After
    public void clear() {
        taskService.removeAll();
        projectService.removeAll();
        taskList.clear();
        userList.clear();
    }

    @Test
    public void testAdd() {
        @NotNull final Project projectDTO = new Project(
                userList.get(0),
                "Test Project",
                "Test Add Project",
                Status.NOT_STARTED);
        projectService.add(projectDTO);
        Assert.assertTrue(projectService.existsById(projectDTO.getUser().getId(), projectDTO.getId()));

        projectService.remove(projectDTO);
    }

    @Test
    public void testAddAll() {
        @NotNull List<Project> projects = new ArrayList<>();
        final long projectSize = projectService.getSize();
        for (int i = 0; i < projectSize; i++) {
            projects.add(new Project(
                    userList.get(0),
                    "Project Test Add " + i,
                    "Project Add Desc " + i,
                    Status.NOT_STARTED));
        }
        projectService.addAll(projects);
        Assert.assertEquals(projectSize * 2, projectService.getSize());

        projectService.removeAll(projects);
    }

    @Test
    public void testCreate() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final String name = "Test_Project_" + user.getLogin();
            @NotNull final String description = "Test_Description_" + user.getLogin();
            @NotNull final Project project = projectService.create(userId, name, description);
            Assert.assertNotNull(project);
            projectService.remove(project);
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateWithNullUser() {
        Assert.assertNotNull(projectService.create(
                null,
                "Test Project",
                "Test Description")
        );
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateWithNullName() {
        Assert.assertNotNull(projectService.create(
                userList.get(0).getId(),
                null,
                "Test Description")
        );
    }

    @Test
    public void testChangeStatusByIdPositive() {
        @Nullable List<Project> projects = projectService.findAll();
        if (projects == null) {
            projectService.addAll(projectList);
            projects = projectList;
        }
        for (@NotNull final Project project : projects) {
            @Nullable final String userId = project.getUser().getId();
            @NotNull final String id = project.getId();
            projectService.changeProjectStatusById(userId, id, Status.COMPLETED);
            @Nullable final Project actualProject = projectService.findOneById(userId, id);
            Assert.assertNotNull(actualProject);
            @NotNull final Status actualStatus = actualProject.getStatus();
            Assert.assertEquals(Status.COMPLETED, actualStatus);
        }
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testChangeStatusByIdNegative() {
        @NotNull final String userId = "Other_user_id";
        @NotNull final String id = "Other_id";
        projectService.changeProjectStatusById(userId, id, Status.COMPLETED);
    }

    @Test
    public void testChangeStatusByIndexPositive() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Project> userProjects = projectService.findAll(userId);
            for (int i = 0; i < userProjects.size(); i++) {
                @Nullable final Project project = projectService.findOneByIndex(userId, i);
                Assert.assertNotNull(project);
                projectService.changeProjectStatusByIndex(userId, i, Status.IN_PROGRESS);
                @Nullable final Project actualProjectDTO = projectService.findOneById(userId, project.getId());
                @NotNull final Status status = actualProjectDTO.getStatus();
                Assert.assertEquals(Status.IN_PROGRESS, status);
            }
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeStatusByIndexNegative() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Project> userProjects = projectService.findAll(userId);
            projectService.changeProjectStatusByIndex(userId, userProjects.size() + 1, Status.IN_PROGRESS);
        }
    }

    @Test
    public void testExistsByIdPositive() {
        for (@NotNull final Project project : projectList) {
            @NotNull final String id = project.getId();
            @NotNull final String user_id = project.getUser().getId();
            Assert.assertTrue(projectService.existsById(user_id, id));
        }
    }

    @Test
    public void testExistsByIdNegative() {
        Assert.assertFalse(projectService.existsById("Other_user_id", "Other_Id"));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Project> projects = projectService.findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectList.size(), projects.size());
    }

    @Test
    public void testFindAllByUserId() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            Assert.assertNotNull(userId);
            @NotNull final List<Project> actualProjects = projectService.findAll(userId);
            @NotNull final List<Project> expectedProjects = projectList.stream()
                    .filter(m -> userId.equals(m.getUser().getId()))
                    .collect(Collectors.toList());
            Assert.assertEquals(actualProjects.size(), expectedProjects.size());
            for (int i = 0; i < actualProjects.size(); i++) {
                Assert.assertEquals(actualProjects.get(i).getId(), expectedProjects.get(i).getId());
            }
        }
    }

    @Test
    public void testFindOneByIdPositive() {
        for (@NotNull final Project project : projectList) {
            @NotNull final String id = project.getId();
            @NotNull final String user_id = project.getUser().getId();
            Assert.assertEquals(project.getId(), projectService.findOneById(user_id, id).getId());
        }
    }

    @Test
    public void testFindOneByIdNegative() {
        @NotNull final String id = "Other_id";
        @NotNull final String user_id = "Other_user_id";
        @Nullable final Project project = projectService.findOneById(user_id, id);
        Assert.assertNull(project);
    }

    @Test
    public void testFindOneByIdUserPositive() {
        for (@NotNull final Project project : projectList) {
            @NotNull final String id = project.getId();
            @Nullable final String userId = project.getUser().getId();
            Assert.assertEquals(project.getId(), projectService.findOneById(userId, id).getId());
        }
    }

    @Test
    public void testFindOneByIdForUserNegative() {
        @NotNull final String id = "Other_Id";
        @NotNull final String userId = "Other_UserId";
        @Nullable final Project project = projectService.findOneById(userId, id);
        Assert.assertNull(project);
    }

    @Test
    public void testFindByIndexForUserPositive() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Project> projects = projectList.stream()
                    .filter(m -> userId.equals(m.getUser().getId()))
                    .collect(Collectors.toList());
            for (int i = 0; i < projects.size(); i++) {
                Assert.assertNotNull(projectService.findOneByIndex(userId, i));
                @Nullable final Project actualProject = projectService.findOneByIndex(userId, i);
                Assert.assertNotNull(actualProject);
                @Nullable final Project expectedProject = projects.get(i);
                Assert.assertEquals(expectedProject.getId(), actualProject.getId());
            }
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIndexForUserNegative() {
        @NotNull final String userId = "Other_User_Id";
        final int index = projectList.size() + 1;
        Assert.assertNull(projectService.findOneByIndex(userId, index));
    }

    @Test
    public void testGetSize() {
        final long projectSize = projectService.getSize();
        Assert.assertEquals(projectList.size(), projectSize);
    }

    @Test
    public void testGetSizeForUser() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            final long actualProjectSize = projectService.getSize(userId);
            @NotNull final List<Project> expectedProjects = projectList.stream()
                    .filter(m -> userId.equals(m.getUser().getId()))
                    .collect(Collectors.toList());
            final long expectedProjectSize = expectedProjects.size();
            Assert.assertEquals(expectedProjectSize, actualProjectSize);
        }
    }

    @Test
    public void testUpdateByIdPositive() {
        for (@NotNull final Project project : projectList) {
            @Nullable final String userId = project.getUser().getId();
            Assert.assertNotNull(userId);
            @NotNull final String id = project.getId();
            @NotNull final String name = "Project Update";
            @NotNull final String description = "Description Update";
            @Nullable final Project actualProjectDTO = projectService.updateById(userId, id, name, description);
            Assert.assertEquals(name, actualProjectDTO.getName());
            Assert.assertEquals(description, actualProjectDTO.getDescription());
        }
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testUpdateByIdNegative() {
        @NotNull final String userId = "Other_User_Id";
        @NotNull final String id = "Other_id";
        @NotNull final String name = "Project Update";
        @NotNull final String description = "Test Update";
        Assert.assertNotNull(projectService.updateById(userId, id, name, description));
    }

    @Test
    public void testUpdateByIndexPositive() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Project> projects = projectList.stream()
                    .filter(m -> userId.equals(m.getUser().getId()))
                    .collect(Collectors.toList());
            for (int i = 0; i < projects.size(); i++) {
                @NotNull final String name = "Project Test Update " + i;
                @NotNull final String description = "Description Test Update " + i;
                @Nullable final Project project = projectService.findOneByIndex(userId, i);
                @NotNull final String project_user_id = project.getUser().getId();
                @NotNull final String project_id = project.getId();
                projectService.updateByIndex(userId, i, name, description);
                @Nullable final Project actualProjectDTO = projectService.findOneById(project_user_id, project_id);
                Assert.assertEquals(name, actualProjectDTO.getName());
                Assert.assertEquals(description, actualProjectDTO.getDescription());
            }
        }
    }

    @Test
    public void testRemoveOneByIdPositive() {
        for (@NotNull final Project project : projectList) {
            @NotNull final String id = project.getId();
            @NotNull final String user_id = project.getUser().getId();
            projectService.removeOneById(user_id, id);
            Assert.assertNull(projectService.findOneById(user_id, id));
        }
    }

    @Test
    public void testRemoveAll() {
        @Nullable List<Project> projectsBcp = projectService.findAll();
        projectService.removeAll();
        Assert.assertEquals(0, projectService.getSize());
        if (projectsBcp != null) projectService.addAll(projectsBcp);
    }

    @Test
    public void testRemoveAllByUserIdPositive() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @Nullable List<Project> projectsBcp = projectService.findAll(userId);
            projectService.removeAllByUserId(userId);
            Assert.assertEquals(0, projectService.getSize(userId));
            projectService.addAll(projectsBcp);
        }
    }

}
