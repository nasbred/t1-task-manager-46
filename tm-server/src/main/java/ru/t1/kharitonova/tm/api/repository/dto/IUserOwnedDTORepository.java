package ru.t1.kharitonova.tm.api.repository.dto;

import ru.t1.kharitonova.tm.dto.model.AbstractUserOwnedModelDTO;

public interface IUserOwnedDTORepository<M extends AbstractUserOwnedModelDTO>
        extends IAbstractDTORepository<M> {

}
