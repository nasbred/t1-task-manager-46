package ru.t1.kharitonova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    void bindTaskToProject(@Nullable String projectId, @Nullable String taskId);

    void changeStatusById(@Nullable String id, @Nullable Status status);

    boolean existsById(@NotNull final String id);

    @Nullable
    Task findOneById(@NotNull String id);

    @Nullable
    Task findOneByIdAndUserId(@NotNull String userId, @NotNull String id);

    @Nullable
    Task findOneByIndex(@NotNull Integer index);

    @Nullable
    Task findOneByIndexByUserId(
            @NotNull String user_id,
            @NotNull Integer index
    );

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAllByUserId(@NotNull String userId);

    @NotNull
    List<Task> findAllByUserIdWithSort(@NotNull String userId, @NotNull String sort);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    long getSize();

    long getSizeForUser(@NotNull String user_id);

    void removeOneById(@NotNull String id);

    void removeOneByIndex(@NotNull Integer index);

    void removeOneByIndexForUser(@NotNull String userId, @NotNull Integer index);

    void removeAll();

    void removeAllByList(@NotNull List<Task> models);

    void removeAllByUserId(@NotNull String userId);

    void unbindTaskFromProject(@Nullable String taskId);

}
