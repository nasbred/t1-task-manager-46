package ru.t1.kharitonova.tm.service;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.api.property.IDatabaseProperty;
import ru.t1.kharitonova.tm.api.service.IConnectionService;
import ru.t1.kharitonova.tm.dto.model.ProjectDTO;
import ru.t1.kharitonova.tm.dto.model.SessionDTO;
import ru.t1.kharitonova.tm.dto.model.TaskDTO;
import ru.t1.kharitonova.tm.dto.model.UserDTO;
import ru.t1.kharitonova.tm.model.Project;
import ru.t1.kharitonova.tm.model.Session;
import ru.t1.kharitonova.tm.model.Task;
import ru.t1.kharitonova.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IDatabaseProperty databaseProperties;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;


    public ConnectionService(@NotNull final IDatabaseProperty databaseProperties) {
        this.databaseProperties = databaseProperties;
        this.entityManagerFactory = getEntityManagerFactory();
    }

    @NotNull
    @Override
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @Override
    @NotNull
    public EntityManagerFactory getEntityManagerFactory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, databaseProperties.getDatabaseDriver());
        settings.put(Environment.URL, databaseProperties.getDatabaseUrl());
        settings.put(Environment.USER, databaseProperties.getDatabaseUsername());
        settings.put(Environment.PASS, databaseProperties.getDatabasePassword());
        settings.put(Environment.DIALECT, databaseProperties.getDBDialect());
        settings.put(Environment.HBM2DDL_AUTO, databaseProperties.getDBHbm2ddlAuto());
        settings.put(Environment.SHOW_SQL, databaseProperties.getDBShowSql());
        settings.put(Environment.FORMAT_SQL, databaseProperties.getDBFormatSql());
        settings.put(Environment.USE_SQL_COMMENTS, databaseProperties.getDBCommentsSql());

        settings.put(Environment.USE_SECOND_LEVEL_CACHE, databaseProperties.getDBSecondLvlCash());
        settings.put(Environment.CACHE_REGION_FACTORY, databaseProperties.getDBFactoryClass());
        settings.put(Environment.USE_QUERY_CACHE, databaseProperties.getDBUseQueryCash());
        settings.put(Environment.USE_MINIMAL_PUTS, databaseProperties.getDBUseMinPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, databaseProperties.getDBRegionPrefix());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, databaseProperties.getDBHazelConfig());

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(ProjectDTO.class);
        source.addAnnotatedClass(TaskDTO.class);
        source.addAnnotatedClass(UserDTO.class);
        source.addAnnotatedClass(SessionDTO.class);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(Task.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }


}
