package ru.t1.kharitonova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.model.ProjectDTO;
import ru.t1.kharitonova.tm.enumerated.ProjectSort;
import ru.t1.kharitonova.tm.enumerated.Status;

import java.util.Collection;
import java.util.List;

public interface IProjectDTOService extends IUserOwnedDTOService<ProjectDTO> {

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    ProjectDTO changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    ProjectDTO changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    long getSize();

    long getSize(@Nullable final String userId);

    boolean existsById(@Nullable final String userId, @Nullable final String id);

    @NotNull
    List<ProjectDTO> findAll();

    @NotNull
    List<ProjectDTO> findAll(@NotNull String userId);

    @NotNull
    List<ProjectDTO> findAll(@NotNull String userId, @Nullable ProjectSort sort);

    @Nullable
    ProjectDTO findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    ProjectDTO findOneByIndex(@Nullable Integer index);

    @Nullable
    ProjectDTO findOneByIndex(@Nullable String userId, @Nullable Integer index);

    void removeOneById(@Nullable String userId, @Nullable String id);

    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    void removeAll();

    void removeAll(@NotNull List<ProjectDTO> models);

    void removeAllByUserId(@Nullable String userId);

    void set(@NotNull Collection<ProjectDTO> projects);

    @NotNull
    ProjectDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    ProjectDTO updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

}
