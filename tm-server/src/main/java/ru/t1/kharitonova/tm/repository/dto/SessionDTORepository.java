package ru.t1.kharitonova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.kharitonova.tm.dto.model.SessionDTO;
import ru.t1.kharitonova.tm.exception.field.IdEmptyException;
import ru.t1.kharitonova.tm.exception.field.UserIdEmptyException;

import javax.persistence.EntityManager;

public class SessionDTORepository extends AbstractUserOwnedDTORepository<SessionDTO>
        implements ISessionDTORepository {

    public SessionDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @Nullable
    public SessionDTO findOneById(@NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        @NotNull final String jpql = "SELECT m FROM SessionDTO m WHERE m.id = :id";
        return entityManager.createQuery(jpql, SessionDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public SessionDTO findOneByUserId(@NotNull final String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final String jpql = "SELECT e FROM SessionDTO e WHERE e.userId = :userId";
        return entityManager.createQuery(jpql, SessionDTO.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public long getSize() {
        return (long) entityManager
                .createQuery("SELECT COUNT(1) FROM SessionDTO")
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOneById(@NotNull final String Id) {
        entityManager
                .createQuery("DELETE FROM SessionDTO e WHERE e.id = :id")
                .setParameter("id", Id)
                .executeUpdate();
    }

    @Override
    public void removeAll() {
        entityManager.createQuery("DELETE FROM SessionDTO").executeUpdate();
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        entityManager
                .createQuery("DELETE FROM SessionDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
