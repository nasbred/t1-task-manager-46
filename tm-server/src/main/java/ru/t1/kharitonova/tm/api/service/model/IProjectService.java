package ru.t1.kharitonova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.enumerated.ProjectSort;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.model.Project;

import java.util.List;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Project changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    long getSize();

    long getSize(@Nullable String userId);

    boolean existsById(@Nullable String userId, @Nullable String id);

    @Nullable
    Project findOneById(@Nullable String user_id, @Nullable String id);

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAll(@NotNull String userId);

    @NotNull
    List<Project> findAll(@NotNull String userId, @Nullable ProjectSort sort);

    @Nullable
    Project findOneByIndex(@Nullable Integer index);

    @Nullable
    Project findOneByIndex(@Nullable String userId, @Nullable Integer index);

    void removeOneById(@Nullable String userId, @Nullable String id);

    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    void removeAll();

    void removeAll(@NotNull List<Project> models);

    void removeAllByUserId(@Nullable String userId);

    @NotNull
    Project updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Project updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

}
