package ru.t1.kharitonova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.repository.model.ITaskRepository;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.exception.field.IdEmptyException;
import ru.t1.kharitonova.tm.exception.field.UserIdEmptyException;
import ru.t1.kharitonova.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

public class TaskRepository extends AbstractUserOwnedRepository<Task>
        implements ITaskRepository {

    public TaskRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void bindTaskToProject(@Nullable final String projectId, @Nullable final String taskId) {
        entityManager
                .createQuery("UPDATE Task e SET e.project.id = :projectId WHERE e.id = :taskId")
                .setParameter("taskId", taskId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public void changeStatusById(@Nullable final String id, @Nullable final Status status) {
        entityManager
                .createQuery("UPDATE Task e SET status = :status WHERE e.id = :id")
                .setParameter("id", id)
                .setParameter("status", status)
                .executeUpdate();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    @Nullable
    public Task findOneById(@NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.id = :id";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public Task findOneByIdAndUserId(@NotNull final String user_id, @NotNull final String id) {
        if (user_id.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        @NotNull final String jpql = "SELECT e FROM Task e WHERE e.user.id = :user_id AND e.id = :id";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("user_id", user_id)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public Task findOneByIndex(@NotNull final Integer index) {
        @NotNull final String jpql = "SELECT e FROM Task e OFFSET :index LIMIT 1";
        return entityManager.createQuery(jpql, Task.class)
                .setMaxResults(index + 1)
                .getResultList()
                .stream()
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public Task findOneByIndexByUserId(@NotNull final String user_id, @NotNull final Integer index) {
        if (user_id.isEmpty()) throw new UserIdEmptyException();
        @NotNull final String jpql = "SELECT e FROM Task e WHERE e.user.id = :user_id";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("user_id", user_id)
                .setMaxResults(index + 1)
                .getResultList()
                .stream()
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        return entityManager.createQuery("FROM Task", Task.class).getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllByUserId(@NotNull final String user_id) {
        return entityManager
                .createQuery("FROM Task e WHERE e.user.id = :user_id", Task.class)
                .setParameter("user_id", user_id)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String user_id, @NotNull final String project_id) {
        @NotNull final String jpql = "SELECT e FROM Task e WHERE e.project.id = :project_id AND e.user.id = :user_id";
        return entityManager
                .createQuery(jpql, Task.class)
                .setParameter("project_id", project_id)
                .setParameter("user_id", user_id)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllByUserIdWithSort(@NotNull final String userId, @NotNull final String sort) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT e FROM Task e WHERE e.user.id = :userId ORDER BY :sort";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setParameter("sort", sort)
                .getResultList();
    }

    @Override
    public long getSize() {
        return (long) entityManager
                .createQuery("SELECT COUNT(1) FROM Task")
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public long getSizeForUser(@NotNull final String user_id) {
        return (long) entityManager
                .createQuery("SELECT COUNT(1) FROM Task e WHERE e.user.id = :userId")
                .setParameter("userId", user_id)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        entityManager
                .createQuery("DELETE FROM Task e WHERE e.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeOneByIndex(@NotNull final Integer index) {
        entityManager.remove(findOneByIndex(index));
    }

    @Override
    public void removeOneByIndexForUser(@NotNull final String userId, @NotNull final Integer index) {
        entityManager.remove(findOneByIndexByUserId(userId, index));
    }

    @Override
    public void removeAll() {
        entityManager.createQuery("DELETE FROM Task").executeUpdate();
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        entityManager
                .createQuery("DELETE FROM Task e WHERE e.user.id = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void unbindTaskFromProject(@Nullable final String taskId) {
        entityManager
                .createQuery("UPDATE Task e SET e.project.id = :projectId WHERE e.id = :taskId")
                .setParameter("taskId", taskId)
                .setParameter("projectId", null)
                .executeUpdate();
    }

    @Override
    public void removeAllByList(@NotNull final List<Task> models) {
        models.forEach(this::remove);
    }

}
