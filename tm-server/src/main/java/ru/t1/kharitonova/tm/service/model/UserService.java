package ru.t1.kharitonova.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.repository.model.IUserRepository;
import ru.t1.kharitonova.tm.api.service.IConnectionService;
import ru.t1.kharitonova.tm.api.service.IPropertyService;
import ru.t1.kharitonova.tm.api.service.model.IProjectService;
import ru.t1.kharitonova.tm.api.service.model.ITaskService;
import ru.t1.kharitonova.tm.api.service.model.IUserService;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.exception.field.IdEmptyException;
import ru.t1.kharitonova.tm.exception.user.*;
import ru.t1.kharitonova.tm.model.User;
import ru.t1.kharitonova.tm.repository.model.UserRepository;
import ru.t1.kharitonova.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

public class UserService extends AbstractService<User, IUserRepository>
        implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    public UserService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IPropertyService propertyService,
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService
    ) {
        super(connectionService);
        this.propertyService = propertyService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @NotNull
    @Override
    public IUserRepository getRepository(@NotNull final EntityManager entityManager) {
        return new UserRepository(entityManager);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();

        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository repository = getRepository(entityManager);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        try {
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository repository = getRepository(entityManager);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        user.setEmail(email);
        try {
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository repository = getRepository(entityManager);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        if (role != null) user.setRole(role);
        try {
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            return repository.findOneById(id);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            return repository.findByLogin(login);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            return repository.findByEmail(email);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<User> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public long getSize() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository repository = getRepository(entityManager);
        try {
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user;
        try {
            user = findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            remove(user);
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user;
        try {
            user = findByEmail(email);
            if (user == null) throw new UserNotFoundException();
            remove(user);
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return user;
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository repository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeAll();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Override
    public void setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository repository = getRepository(entityManager);
        try {
            @Nullable final String password_hash = HashUtil.salt(propertyService, password);
            if (password_hash == null) throw new PasswordEmptyException();
            entityManager.getTransaction().begin();
            repository.setPassword(id, password_hash);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public User lockUserByLogin(@Nullable final String login) {
        if (login == null) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository repository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.lockUserById(user.getId());
            entityManager.getTransaction().commit();
            return repository.findOneById(user.getId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public User unlockUserByLogin(@Nullable final String login) {
        if (login == null) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository repository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.unLockUserById(user.getId());
            entityManager.getTransaction().commit();
            return repository.findOneById(user.getId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
