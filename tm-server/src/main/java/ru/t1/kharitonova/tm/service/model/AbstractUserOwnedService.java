package ru.t1.kharitonova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.kharitonova.tm.api.service.IConnectionService;
import ru.t1.kharitonova.tm.api.service.model.IUserOwnedService;
import ru.t1.kharitonova.tm.model.AbstractUserOwnedModel;

import javax.persistence.EntityManager;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R>
        implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

}
