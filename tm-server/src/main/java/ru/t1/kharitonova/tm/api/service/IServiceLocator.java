package ru.t1.kharitonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.api.service.dto.IProjectDTOService;
import ru.t1.kharitonova.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.kharitonova.tm.api.service.dto.ITaskDTOService;
import ru.t1.kharitonova.tm.api.service.dto.IUserDTOService;

public interface IServiceLocator {

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectDTOService getProjectDTOService();

    @NotNull
    IProjectTaskDTOService getProjectTaskDTOService();

    @NotNull
    ITaskDTOService getTaskDTOService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IUserDTOService getUserDTOService();

    @NotNull
    IDomainService getDomainService();

}
