package ru.t1.kharitonova.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.repository.model.ISessionRepository;
import ru.t1.kharitonova.tm.api.service.IConnectionService;
import ru.t1.kharitonova.tm.api.service.model.ISessionService;
import ru.t1.kharitonova.tm.exception.entity.ModelNotFoundException;
import ru.t1.kharitonova.tm.exception.entity.SessionNotFoundException;
import ru.t1.kharitonova.tm.exception.field.IdEmptyException;
import ru.t1.kharitonova.tm.model.Session;
import ru.t1.kharitonova.tm.repository.model.SessionRepository;

import javax.persistence.EntityManager;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository>
        implements ISessionService {

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @NotNull
    protected ISessionRepository getRepository(@NotNull EntityManager entityManager) {
        return new SessionRepository(entityManager);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session create(@Nullable final Session session) {
        if (session == null) throw new ModelNotFoundException();
        @Nullable final Session result;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.add(session);
            result = repository.findOneById(session.getId());
            if (result == null) throw new SessionNotFoundException();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public Boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            return repository.findOneById(id);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOneByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            return repository.findOneByUserId(userId);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public long getSize() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeOneById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeAll();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
