package ru.t1.kharitonova.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.kharitonova.tm.api.service.IConnectionService;
import ru.t1.kharitonova.tm.api.service.dto.IUserOwnedDTOService;
import ru.t1.kharitonova.tm.dto.model.AbstractUserOwnedModelDTO;

import javax.persistence.EntityManager;

public abstract class AbstractUserOwnedDTOService<M extends AbstractUserOwnedModelDTO, R extends IUserOwnedDTORepository<M>>
        extends AbstractDTOService<M, R>
        implements IUserOwnedDTOService<M> {

    public AbstractUserOwnedDTOService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

}
