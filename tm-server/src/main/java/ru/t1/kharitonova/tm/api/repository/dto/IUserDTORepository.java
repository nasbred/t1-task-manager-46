package ru.t1.kharitonova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserDTORepository extends IAbstractDTORepository<UserDTO> {

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    @Nullable
    UserDTO findByEmail(@NotNull String email);

    @Nullable
    UserDTO findOneById(@Nullable String id);

    @NotNull
    List<UserDTO> findAll();

    long getSize();

    void setPassword(@Nullable String id, @NotNull String passwordHash);

    void updateUser(@Nullable String id,
                    @Nullable String firstName,
                    @Nullable String lastName,
                    @Nullable String middleName);

    void lockUserById(@Nullable String id);

    void unLockUserById(@Nullable String id);

    void removeAll();

}
