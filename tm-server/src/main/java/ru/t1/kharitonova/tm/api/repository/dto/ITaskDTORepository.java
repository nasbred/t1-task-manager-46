package ru.t1.kharitonova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.model.TaskDTO;
import ru.t1.kharitonova.tm.enumerated.Status;

import java.util.List;

public interface ITaskDTORepository extends IUserOwnedDTORepository<TaskDTO> {

    void bindTaskToProject(@Nullable String projectId, @Nullable String taskId);

    void changeStatusById(@Nullable String id, @Nullable Status status);

    boolean existsById(@NotNull final String id);

    @Nullable
    TaskDTO findOneById(@NotNull String id);

    @Nullable
    TaskDTO findOneByIdAndUserId(@NotNull String userId, @NotNull String id);

    @Nullable
    TaskDTO findOneByIndex(@NotNull Integer index);

    @Nullable
    TaskDTO findOneByIndexByUserId(
            @NotNull String user_id,
            @NotNull Integer index
    );

    @NotNull
    List<TaskDTO> findAll();

    @NotNull
    List<TaskDTO> findAllByUserId(@NotNull String userId);

    @NotNull
    List<TaskDTO> findAllByUserIdWithSort(@NotNull String userId, @NotNull String sort);

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String projectId);

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    long getSize();

    long getSizeForUser(@NotNull String user_id);

    void removeOneById(@NotNull String id);

    void removeOneByIndex(@NotNull Integer index);

    void removeOneByIndexForUser(@NotNull String userId, @NotNull Integer index);

    void removeAll();

    void removeAllByList(@NotNull List<TaskDTO> models);

    void removeAllByUserId(@NotNull String userId);

    void unbindTaskFromProject(@Nullable String taskId);

}
