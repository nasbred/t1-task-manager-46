package ru.t1.kharitonova.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractUserOwnedModelDTO extends AbstractModelDTO {

    @NotNull
    @Column(name = "user_id", nullable = false)
    private String userId;

    public AbstractUserOwnedModelDTO(@NotNull final String userId) {
        this.userId = userId;
    }

}
