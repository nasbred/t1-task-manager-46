package ru.t1.kharitonova.tm.exception.user;

import ru.t1.kharitonova.tm.exception.AbstractException;

public class LoginEmptyException extends AbstractException {

    public LoginEmptyException() {
        super("Error! Login is empty.");
    }

}
