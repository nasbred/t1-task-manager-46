package ru.t1.kharitonova.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.model.IWBS;
import ru.t1.kharitonova.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "tm_task")
@NoArgsConstructor
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class TaskDTO extends AbstractUserOwnedModelDTO implements IWBS {

    @NotNull
    @Column(name = "name", nullable = false)
    private String name = "";

    @Nullable
    @Column(name = "description")
    private String description = "";

    @NotNull
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    @NotNull
    @Column(name = "created", nullable = false)
    private Date created = new Date();

    public TaskDTO(
            @NotNull final String userId,
            @NotNull final String name,
            @Nullable final String description,
            @NotNull final Status status,
            @Nullable final String projectId
    ) {
        super(userId);
        this.name = name;
        this.description = description;
        this.status = status;
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return name + " : " + description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskDTO taskDTO = (TaskDTO) o;
        return name.equals(taskDTO.name) &&
                Objects.equals(description, taskDTO.description) &&
                status == taskDTO.status &&
                Objects.equals(projectId, taskDTO.projectId) &&
                created.equals(taskDTO.created);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, status, projectId, created);
    }
}
