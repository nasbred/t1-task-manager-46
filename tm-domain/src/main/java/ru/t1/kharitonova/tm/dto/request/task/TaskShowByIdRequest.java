package ru.t1.kharitonova.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskShowByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public TaskShowByIdRequest(@Nullable final String token) {
        super(token);
    }

    public TaskShowByIdRequest(@Nullable final String token, @Nullable final String id) {
        super(token);
        this.id = id;
    }

}
